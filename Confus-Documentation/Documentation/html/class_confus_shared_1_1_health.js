var class_confus_shared_1_1_health =
[
    [ "Health", "class_confus_shared_1_1_health.html#abe53d39fc88b4ba34b330684e3c70176", null ],
    [ "damage", "class_confus_shared_1_1_health.html#a0ecfa64051258d5237341e6088edccc3", null ],
    [ "getHealth", "class_confus_shared_1_1_health.html#a11b1d28af82501486f6ce07014854e6c", null ],
    [ "heal", "class_confus_shared_1_1_health.html#a05fa6161b24de20d2aee73cf02c4854e", null ],
    [ "reset", "class_confus_shared_1_1_health.html#a46a5e55d7e285e49b57795bfac38e914", null ],
    [ "setDeathCallback", "class_confus_shared_1_1_health.html#adbfc0b7fadb8cf793c928bbabed2670b", null ],
    [ "DamageEvent", "class_confus_shared_1_1_health.html#acd331640d8b0837b774635309f640849", null ]
];