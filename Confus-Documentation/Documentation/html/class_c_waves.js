var class_c_waves =
[
    [ "CWaves", "class_c_waves.html#aae3d19f15234873089e59fd5a6924efa", null ],
    [ "~CWaves", "class_c_waves.html#a70935c16f8b7bee1c53b3049fa4da925", null ],
    [ "DeleteWaveFile", "class_c_waves.html#a9af3ea27a4f1b8d8bfb2fbe0d3ee0519", null ],
    [ "GetErrorString", "class_c_waves.html#a69b1f6cd34fddb3725efd5ec22bea109", null ],
    [ "GetWaveALBufferFormat", "class_c_waves.html#a1e3439c106447637b024b17d1309cb2d", null ],
    [ "GetWaveData", "class_c_waves.html#a2af0af8b9f42b22b37bd2b070f185439", null ],
    [ "GetWaveDataOffset", "class_c_waves.html#a80d060bad712a16fa58cc4e14ee1e168", null ],
    [ "GetWaveFormatExHeader", "class_c_waves.html#a37400a62934bc2c98340246aa2c25e1c", null ],
    [ "GetWaveFormatExtensibleHeader", "class_c_waves.html#a9cff9340258652a6347f1838d7ab6cb3", null ],
    [ "GetWaveFrequency", "class_c_waves.html#a34855f6ae8d933355241cbd093627dc7", null ],
    [ "GetWaveSize", "class_c_waves.html#ae65d2fe770dacfeee01320236395243f", null ],
    [ "GetWaveType", "class_c_waves.html#abfe78a2518c09b2ccf4d3dcd52c0bb1b", null ],
    [ "IsWaveID", "class_c_waves.html#ade2c151bd6dd0113bbd5e48a0034fa5d", null ],
    [ "LoadWaveFile", "class_c_waves.html#ab04163eed493b9ad3a8018f9d59ad737", null ],
    [ "OpenWaveFile", "class_c_waves.html#a5a1e2a7c6f6286951e29b4a93b5e318e", null ],
    [ "ReadWaveData", "class_c_waves.html#aee9f92e9d35e6155fd383a34a95b480a", null ],
    [ "SetWaveDataOffset", "class_c_waves.html#ae810545259a7ac689906019892cb8b7f", null ]
];