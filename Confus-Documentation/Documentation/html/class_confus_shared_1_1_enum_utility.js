var class_confus_shared_1_1_enum_utility =
[
    [ "bitwiseEnumAND", "class_confus_shared_1_1_enum_utility.html#a2472eb2b4f27046763d99af88236f4ea", null ],
    [ "bitwiseEnumComplement", "class_confus_shared_1_1_enum_utility.html#ae0a4164e4193dda639e37cd739748342", null ],
    [ "bitwiseEnumOR", "class_confus_shared_1_1_enum_utility.html#a4d510c93196b945a88370e7bf545468d", null ],
    [ "bitwiseEnumXOR", "class_confus_shared_1_1_enum_utility.html#a49e4ed6e5a4906806e45130eae26806d", null ],
    [ "hasFlag", "class_confus_shared_1_1_enum_utility.html#ace4712b518fa6a2260e689becdb75356", null ]
];