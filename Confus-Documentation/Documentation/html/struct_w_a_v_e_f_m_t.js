var struct_w_a_v_e_f_m_t =
[
    [ "guidSubFormat", "struct_w_a_v_e_f_m_t.html#a814b9aa637240c8cb547b92b58ca919f", null ],
    [ "ulAvgBytesPerSec", "struct_w_a_v_e_f_m_t.html#a748324e3d30be470d04902b7a92d9d13", null ],
    [ "ulChannelMask", "struct_w_a_v_e_f_m_t.html#aac3aebefa1f1790ab3e7f5e3828c9bf3", null ],
    [ "ulSamplesPerSec", "struct_w_a_v_e_f_m_t.html#a416ea2301dbe3dbacfe0bfea9a65eb12", null ],
    [ "usBitsPerSample", "struct_w_a_v_e_f_m_t.html#ab5eee50791803915a9f78c953710fb30", null ],
    [ "usBlockAlign", "struct_w_a_v_e_f_m_t.html#a208aedbe61313b28456bc3dfaebd5ea3", null ],
    [ "usChannels", "struct_w_a_v_e_f_m_t.html#a70afdc0f38b03403f5e9ef3561bc504e", null ],
    [ "usFormatTag", "struct_w_a_v_e_f_m_t.html#a9749e82026938f8cfc3121bffe01f9bf", null ],
    [ "usReserved", "struct_w_a_v_e_f_m_t.html#a1a6a9a5222335fdf464671f4e8bf7078", null ],
    [ "usSize", "struct_w_a_v_e_f_m_t.html#a7e7e5409ad17308a07035176b841ce0f", null ]
];