var class_confus_server_1_1_team_score =
[
    [ "TeamScore", "class_confus_server_1_1_team_score.html#a12d2ad97d99840a8ff81dce0960851a8", null ],
    [ "getPointsOfTeam", "class_confus_server_1_1_team_score.html#a0c6fc820eec178d1e83cb572bfb9298b", null ],
    [ "resetScore", "class_confus_server_1_1_team_score.html#a13fcee5a947ec3cf38faeb8a4c3153f9", null ],
    [ "sendScoreToClients", "class_confus_server_1_1_team_score.html#a8563a217616a7809e002b911a6f05c53", null ],
    [ "setConnection", "class_confus_server_1_1_team_score.html#ad4b3d5e49e4be9d8a5b530ed19967506", null ],
    [ "setResetCallback", "class_confus_server_1_1_team_score.html#ac19aacb804e62dd2c50af425798dda96", null ]
];