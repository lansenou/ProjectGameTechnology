var annotated_dup =
[
    [ "Confus", null, [
      [ "Audio", null, [
        [ "OpenAL", null, [
          [ "ALDEVICEINFO", "struct_confus_1_1_audio_1_1_open_a_l_1_1_a_l_d_e_v_i_c_e_i_n_f_o.html", "struct_confus_1_1_audio_1_1_open_a_l_1_1_a_l_d_e_v_i_c_e_i_n_f_o" ],
          [ "ALDeviceList", "class_confus_1_1_audio_1_1_open_a_l_1_1_a_l_device_list.html", "class_confus_1_1_audio_1_1_open_a_l_1_1_a_l_device_list" ],
          [ "OpenALBuffer", "class_confus_1_1_audio_1_1_open_a_l_1_1_open_a_l_buffer.html", "class_confus_1_1_audio_1_1_open_a_l_1_1_open_a_l_buffer" ],
          [ "OPENALFNTABLE", "struct_confus_1_1_audio_1_1_open_a_l_1_1_o_p_e_n_a_l_f_n_t_a_b_l_e.html", "struct_confus_1_1_audio_1_1_open_a_l_1_1_o_p_e_n_a_l_f_n_t_a_b_l_e" ],
          [ "OpenALListener", "class_confus_1_1_audio_1_1_open_a_l_1_1_open_a_l_listener.html", "class_confus_1_1_audio_1_1_open_a_l_1_1_open_a_l_listener" ],
          [ "OpenALSource", "class_confus_1_1_audio_1_1_open_a_l_1_1_open_a_l_source.html", "class_confus_1_1_audio_1_1_open_a_l_1_1_open_a_l_source" ]
        ] ],
        [ "AudioManager", "class_confus_1_1_audio_1_1_audio_manager.html", "class_confus_1_1_audio_1_1_audio_manager" ],
        [ "PlayerAudioEmitter", "class_confus_1_1_audio_1_1_player_audio_emitter.html", "class_confus_1_1_audio_1_1_player_audio_emitter" ],
        [ "Sound", "class_confus_1_1_audio_1_1_sound.html", "class_confus_1_1_audio_1_1_sound" ]
      ] ],
      [ "Networking", null, [
        [ "ClientConnection", "class_confus_1_1_networking_1_1_client_connection.html", "class_confus_1_1_networking_1_1_client_connection" ]
      ] ],
      [ "Announcer", "class_confus_1_1_announcer.html", "class_confus_1_1_announcer" ],
      [ "ClientTeamScore", "class_confus_1_1_client_team_score.html", "class_confus_1_1_client_team_score" ],
      [ "FlagGUI", "class_confus_1_1_flag_g_u_i.html", "class_confus_1_1_flag_g_u_i" ],
      [ "Game", "class_confus_1_1_game.html", "class_confus_1_1_game" ],
      [ "GUI", "class_confus_1_1_g_u_i.html", "class_confus_1_1_g_u_i" ],
      [ "LocalPlayerController", "class_confus_1_1_local_player_controller.html", "class_confus_1_1_local_player_controller" ],
      [ "Menu", "class_confus_1_1_menu.html", "class_confus_1_1_menu" ],
      [ "RemotePlayerController", "class_confus_1_1_remote_player_controller.html", "class_confus_1_1_remote_player_controller" ],
      [ "ScoreGUI", "class_confus_1_1_score_g_u_i.html", "class_confus_1_1_score_g_u_i" ],
      [ "WinScreen", "class_confus_1_1_win_screen.html", "class_confus_1_1_win_screen" ]
    ] ],
    [ "ConfusServer", null, [
      [ "Networking", null, [
        [ "Connection", "class_confus_server_1_1_networking_1_1_connection.html", "class_confus_server_1_1_networking_1_1_connection" ]
      ] ],
      [ "Game", "class_confus_server_1_1_game.html", "class_confus_server_1_1_game" ],
      [ "RemoteInputReceiver", "class_confus_server_1_1_remote_input_receiver.html", "class_confus_server_1_1_remote_input_receiver" ],
      [ "TeamScore", "class_confus_server_1_1_team_score.html", "class_confus_server_1_1_team_score" ]
    ] ],
    [ "ConfusShared", null, [
      [ "Physics", null, [
        [ "BoxCollider", "class_confus_shared_1_1_physics_1_1_box_collider.html", "class_confus_shared_1_1_physics_1_1_box_collider" ],
        [ "Collider", "class_confus_shared_1_1_physics_1_1_collider.html", "class_confus_shared_1_1_physics_1_1_collider" ],
        [ "CollisionRegistrar", "class_confus_shared_1_1_physics_1_1_collision_registrar.html", "class_confus_shared_1_1_physics_1_1_collision_registrar" ],
        [ "DebugDrawer", "class_confus_shared_1_1_physics_1_1_debug_drawer.html", "class_confus_shared_1_1_physics_1_1_debug_drawer" ],
        [ "PhysicsWorld", "class_confus_shared_1_1_physics_1_1_physics_world.html", "class_confus_shared_1_1_physics_1_1_physics_world" ],
        [ "RigidBody", "class_confus_shared_1_1_physics_1_1_rigid_body.html", "class_confus_shared_1_1_physics_1_1_rigid_body" ]
      ] ],
      [ "BaseGame", "class_confus_shared_1_1_base_game.html", "class_confus_shared_1_1_base_game" ],
      [ "Delegate", "class_confus_shared_1_1_delegate.html", "class_confus_shared_1_1_delegate" ],
      [ "EnumUtility", "class_confus_shared_1_1_enum_utility.html", "class_confus_shared_1_1_enum_utility" ],
      [ "EventManager", "class_confus_shared_1_1_event_manager.html", "class_confus_shared_1_1_event_manager" ],
      [ "Flag", "class_confus_shared_1_1_flag.html", "class_confus_shared_1_1_flag" ],
      [ "Health", "class_confus_shared_1_1_health.html", "class_confus_shared_1_1_health" ],
      [ "Maze", "class_confus_shared_1_1_maze.html", "class_confus_shared_1_1_maze" ],
      [ "MazeGenerator", "class_confus_shared_1_1_maze_generator.html", "class_confus_shared_1_1_maze_generator" ],
      [ "MazeTile", "class_confus_shared_1_1_maze_tile.html", "class_confus_shared_1_1_maze_tile" ],
      [ "MoveableWall", "class_confus_shared_1_1_moveable_wall.html", "class_confus_shared_1_1_moveable_wall" ],
      [ "Player", "class_confus_shared_1_1_player.html", "class_confus_shared_1_1_player" ],
      [ "PlayerInputState", "struct_confus_shared_1_1_player_input_state.html", "struct_confus_shared_1_1_player_input_state" ],
      [ "RespawnFloor", "class_confus_shared_1_1_respawn_floor.html", "class_confus_shared_1_1_respawn_floor" ],
      [ "WalledMazeTile", "class_confus_shared_1_1_walled_maze_tile.html", "class_confus_shared_1_1_walled_maze_tile" ],
      [ "Weapon", "class_confus_shared_1_1_weapon.html", "class_confus_shared_1_1_weapon" ]
    ] ],
    [ "btIDebugDraw", "classbt_i_debug_draw.html", null ],
    [ "CWaves", "class_c_waves.html", "class_c_waves" ],
    [ "IUIElement", "class_i_u_i_element.html", "class_i_u_i_element" ],
    [ "RIFFCHUNK", "struct_r_i_f_f_c_h_u_n_k.html", "struct_r_i_f_f_c_h_u_n_k" ],
    [ "tWAVEFORMATEX", "structt_w_a_v_e_f_o_r_m_a_t_e_x.html", "structt_w_a_v_e_f_o_r_m_a_t_e_x" ],
    [ "WAVEFILEHEADER", "struct_w_a_v_e_f_i_l_e_h_e_a_d_e_r.html", "struct_w_a_v_e_f_i_l_e_h_e_a_d_e_r" ],
    [ "WAVEFILEINFO", "struct_w_a_v_e_f_i_l_e_i_n_f_o.html", "struct_w_a_v_e_f_i_l_e_i_n_f_o" ],
    [ "WAVEFMT", "struct_w_a_v_e_f_m_t.html", "struct_w_a_v_e_f_m_t" ],
    [ "WAVEFORMATEXTENSIBLE", "struct_w_a_v_e_f_o_r_m_a_t_e_x_t_e_n_s_i_b_l_e.html", "struct_w_a_v_e_f_o_r_m_a_t_e_x_t_e_n_s_i_b_l_e" ]
];